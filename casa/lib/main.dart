import 'package:flutter/material.dart';
import 'package:casa/booking.dart';
import 'package:animated_splash_screen/animated_splash_screen.dart';
import 'package:firebase_core/firebase_core.dart';

Future main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp(
      options: FirebaseOptions(
          apiKey: "AIzaSyCuok_fAfd64iXDf83BJA1KECgOllggjxM",
          authDomain: "casa-web-9db2c.firebaseapp.com",
          projectId: "casa-web-9db2c",
          storageBucket: "casa-web-9db2c.appspot.com",
          messagingSenderId: "80446425823",
          appId: "1:80446425823:web:f470253d4f92b72ed8f76a"));
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        scaffoldBackgroundColor: Color.fromARGB(255, 255, 255, 255),
        colorScheme: ColorScheme.fromSwatch(
          primarySwatch: Colors.cyan,
        ).copyWith(
          secondary: Color.fromARGB(255, 144, 224, 239),
        ),
        splashColor: Color.fromARGB(255, 3, 4, 95),
      ),
      home: AnimatedSplashScreen(
        splash: Image.asset(
          'logo.png',
          height: 970,
          width: 865,
        ),
        backgroundColor: Colors.white,
        nextScreen: Bookings(),
        splashTransition: SplashTransition.scaleTransition,
        duration: 2100,
      ),
    );
  }
}
