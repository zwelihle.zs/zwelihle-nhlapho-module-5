import 'package:casa/bookingsList.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';

class Bookings extends StatefulWidget {
  const Bookings({Key? key}) : super(key: key);

  @override
  _BookingsState createState() => _BookingsState();
}

class _BookingsState extends State<Bookings> {
  @override
  Widget build(BuildContext context) {
    TextEditingController nameController = TextEditingController();
    TextEditingController checkinController = TextEditingController();
    TextEditingController checkoutController = TextEditingController();

    Future _addBookings() {
      final name = nameController.text;
      final checkInTime = checkinController.text;
      final checkOutTime = checkoutController.text;

      final ref = FirebaseFirestore.instance.collection("bookings").doc();

      return ref
          .set({
            "Name": name,
            "checkIn": checkInTime,
            "checkOut": checkOutTime,
            "doc_id": ref.id
          })
          .then((value) => {
                nameController.text = "",
                checkinController.text = "",
                checkoutController.text = ""
              })
          .catchError((onError) => log(onError));
    }

    return Scaffold(
      appBar: AppBar(title: Text('Booking')),
      body: SingleChildScrollView(
        //added scrollview because of overlapping
        child: Column(
          children: [
            Container(
              child: const Image(
                image: AssetImage('rm1.jpg'),
                height: 145.0,
              ),
            ),
            Container(
              alignment: Alignment.centerLeft,
              padding: EdgeInsets.zero,
              margin: EdgeInsets.fromLTRB(13.0, 5.0, 0.0, 1.0),
              child: Text(
                'Name',
                textAlign: TextAlign.center,
                style: TextStyle(fontSize: 16, fontWeight: FontWeight.bold),
              ),
            ),
            Padding(
              padding: EdgeInsets.symmetric(horizontal: 6, vertical: 6),
              child: TextField(
                controller: nameController,
                decoration: InputDecoration(
                    border: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(15)),
                    hintText: "e.g Mkabayi Zulu "),
              ),
            ),
            Container(
              alignment: Alignment.centerLeft,
              padding: EdgeInsets.zero,
              margin: EdgeInsets.fromLTRB(13.0, 5.0, 0.0, 1.0),
              child: Text(
                'Check-in time',
                textAlign: TextAlign.center,
                style: TextStyle(fontSize: 16),
              ),
            ),
            Padding(
              padding: EdgeInsets.symmetric(horizontal: 6, vertical: 6),
              child: TextField(
                controller: checkinController,
                decoration: InputDecoration(
                    border: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(15)),
                    hintText: "e.g 02 September 2022"),
              ),
            ),
            Container(
              alignment: Alignment.centerLeft,
              padding: EdgeInsets.zero,
              margin: EdgeInsets.fromLTRB(13.0, 5.0, 0.0, 1.0),
              child: Text(
                'Check-out time',
                textAlign: TextAlign.center,
                style: TextStyle(fontSize: 16),
              ),
            ),
            Padding(
              padding: EdgeInsets.symmetric(horizontal: 6, vertical: 6),
              child: TextField(
                controller: checkoutController,
                decoration: InputDecoration(
                    border: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(15)),
                    hintText: "e.g 04 September 2022"),
              ),
            ),
            ElevatedButton(
                onPressed: () {
                  _addBookings();
                },
                child: Text('Book')),
            BookingsList()
          ],
        ),
      ),
    );
  }

  log(String s) {}
}
