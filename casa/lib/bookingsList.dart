import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:firebase_core/firebase_core.dart';

class BookingsList extends StatefulWidget {
  const BookingsList({Key? key}) : super(key: key);

  @override
  _BookingsListState createState() => _BookingsListState();
}

class _BookingsListState extends State<BookingsList> {
  final Stream<QuerySnapshot> myBookings =
      FirebaseFirestore.instance.collection("bookings").snapshots();

  @override
  Widget build(BuildContext context) {
    TextEditingController nameController = TextEditingController();
    TextEditingController checkInController = TextEditingController();
    TextEditingController checkOutController = TextEditingController();

    void _delete(docId) {
      FirebaseFirestore.instance
          .collection("bookings")
          .doc(docId)
          .delete()
          .then((value) => print("deleted"));
    }

    void _update(data) {
      var collection = FirebaseFirestore.instance.collection("bookings");
      nameController.text = data["Name"];
      checkInController.text = data["checkIn"];
      checkOutController.text = data["checkOut"];
      showDialog(
          context: context,
          builder: (_) => AlertDialog(
                title: Text("Update"),
                content: Column(mainAxisSize: MainAxisSize.min, children: [
                  TextField(
                    controller: nameController,
                  ),
                  TextField(
                    controller: checkInController,
                  ),
                  TextField(
                    controller: checkOutController,
                  ),
                  TextButton(
                      onPressed: () {
                        collection.doc(data["doc_id"]).update({
                          "Name": nameController.text,
                          "checkIn": checkInController.text,
                          "checkOut": checkOutController.text
                        });
                        Navigator.pop(context);
                      },
                      child: Text("Update"))
                ]),
              ));
    }

    return StreamBuilder(
      stream: myBookings,
      builder: (BuildContext context,
          AsyncSnapshot<QuerySnapshot<Object?>> snapshot) {
        if (snapshot.hasError) {
          return Text("It went wrong");
        }

        if (snapshot.connectionState == ConnectionState.waiting) {
          return CircularProgressIndicator();
        }

        if (snapshot.hasData) {
          return Row(
            children: [
              Expanded(
                  child: SizedBox(
                height: (MediaQuery.of(context).size.height),
                width: MediaQuery.of(context).size.width,
                child: ListView(
                  children: snapshot.data!.docs
                      .map((DocumentSnapshot documentSnapshot) {
                    Map<String, dynamic> data =
                        documentSnapshot.data()! as Map<String, dynamic>;
                    return Column(
                      children: [
                        Card(
                          child: Column(
                            children: [
                              Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceEvenly,
                                  children: [
                                    Text(
                                      "Name:\n" + data['Name'],
                                      textScaleFactor: 1.0,
                                    ),
                                    Text(
                                      'Check in date:\n' + data['checkIn'],
                                      textScaleFactor: 1.0,
                                    ),
                                    Text(
                                      'Check out date:\n' + data['checkOut'],
                                      textScaleFactor: 1.0,
                                    ),
                                  ]),
                              Row(
                                children: [
                                  ButtonTheme(
                                      buttonColor:
                                          Color.fromARGB(255, 3, 4, 95),
                                      child: ButtonBar(
                                        children: [
                                          OutlineButton.icon(
                                            onPressed: () {
                                              _update(data);
                                            },
                                            icon: Icon(Icons.edit),
                                            label: Text("Edit"),
                                          )
                                        ],
                                      )),
                                  ButtonTheme(
                                      buttonColor:
                                          Color.fromARGB(255, 3, 4, 95),
                                      child: ButtonBar(
                                        children: [
                                          OutlineButton.icon(
                                            onPressed: () {
                                              _delete(data["doc_id"]);
                                            },
                                            icon: Icon(Icons.remove),
                                            label: Text("Delete"),
                                          )
                                        ],
                                      ))
                                ],
                              ),
                            ],
                          ),
                        ),
                      ],
                    );
                  }).toList(),
                ),
              ))
            ],
          );
        } else {
          return (Text("no data"));
        }
      },
    );
  }
}
